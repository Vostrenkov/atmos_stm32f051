
#include "peripheral.h"


uint16_t crc16(uint8_t *adr_buffer, uint32_t byte_cnt);
void RS485_read_reg(uint8_t addr, uint8_t reg);
void RS485_write_reg(uint8_t addr, uint8_t reg, uint8_t* data, uint8_t len);