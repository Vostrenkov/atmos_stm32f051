
#include "peripheral.h"

uint8_t uart2_transmitter_state = 0;

extern uint32_t timer_flags;
extern struct UART_struct
{
	uint8_t rx_buf[255];
	uint8_t rx_cnt;
}uart2;

struct {
	
	unsigned char Number;
	uint32_t Time;
}SoftTimer[Max_Num_Of_Timers];


void Delay_ms(__IO uint32_t nTime)
{
  TimingDelay = nTime;
  while(TimingDelay != 0);
}


void SetTimer (unsigned char NewNumber, uint32_t NewTime)			// new number это номер бита в флаговой переменной
{
	unsigned char i;
	for (i=0;i!=Max_Num_Of_Timers;i++)			// прочесываем очередь таймеров
	{
		if (SoftTimer[i].Number == NewNumber)	// если таймер с заданным номером уже есть
		{
			SoftTimer[i].Time = NewTime;				// то перезаписываем в нем время
			return;
		}
	}
	for (i=0;i!=Max_Num_Of_Timers;i++)			// иначе ищм пустой таймер 
	{																				// и записываем все в него
		if (SoftTimer[i].Number == 0)
		{
			SoftTimer[i].Number = NewNumber;	
			SoftTimer[i].Time = NewTime;
			return;			
		}
	}
}

void KillTimer (unsigned char Flag)
{
	int i;
	for (i=0;i<Max_Num_Of_Timers;i++)			// ищем заданный таймер
	{
		if (SoftTimer[i].Number == Flag)
		{
			SoftTimer[i].Number = 0;					// ставим в номер заглушку
			return;
		}
	}
}

void SysTick_Handler(void)
{
	unsigned char i;	
	if (TimingDelay != 0x00)										// реализация программной задержки
  {
    TimingDelay--;
  }
	
	for (i=0;i<=Max_Num_Of_Timers;i++)						// реализация таймеров
	{
		if (SoftTimer[i].Number == 0) continue;
		if (SoftTimer[i].Time != 0) SoftTimer[i].Time--;
		else 
		{
			timer_flags	|=	(1<<SoftTimer[i].Number);
			SoftTimer[i].Number = 0;
		}
	}
}
	
void UART1_config (void)
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_1);
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx|USART_Mode_Tx;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART1,&USART_InitStructure);
	
	USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
	//USART_ITConfig(USART1,USART_IT_IDLE,ENABLE);
	
	NVIC_SetPriority(USART1_IRQn,1);
	NVIC_EnableIRQ(USART1_IRQn);
	
	USART_Cmd(USART1,ENABLE);
}

void UART1_send_byte(uint8_t data)
{
	//while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
	while (!(USART1->ISR & (USART_ISR_TXE|USART_ISR_TC)));
	USART_SendData(USART1,data);
}
void UART1_send_str (uint8_t* str)
{
	while (*str)
	{
		UART1_send_byte(*str++);
	}
}

void GPIO_config (void)
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC,ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8|GPIO_Pin_9;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC,&GPIO_InitStructure);
	LED1_OFF;
	LED2_OFF;
}

void UART2_config (void)
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1,ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	DMA_InitTypeDef DMA_InitStructure;
	
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_1);
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx|USART_Mode_Tx;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART2,&USART_InitStructure);
	
	USART_DMACmd(USART2,USART_DMAReq_Rx|USART_DMAReq_Tx,ENABLE);
	//USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);
	USART_ITConfig(USART2,USART_IT_IDLE,ENABLE);
	
	DMA_InitStructure.DMA_BufferSize = 255;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&uart2.rx_buf[0];
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&USART2->RDR;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
	DMA_Init(DMA1_Channel5,&DMA_InitStructure);
	
	DMA_ITConfig(DMA1_Channel4,DMA_IT_TC,ENABLE);
	
	NVIC_SetPriority(DMA1_Channel4_5_IRQn,4);
	NVIC_EnableIRQ(DMA1_Channel4_5_IRQn);
	NVIC_SetPriority(USART2_IRQn,3);
	NVIC_EnableIRQ(USART2_IRQn);

	USART_Cmd(USART2,ENABLE);
	DMA_Cmd(DMA1_Channel5,ENABLE);
}

void UART2_send_byte(uint8_t data)
{
	//while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
	while (!(USART2->ISR & (USART_ISR_TXE|USART_ISR_TC)));
	USART_SendData(USART2,data);
}
void UART2_send_str (uint8_t* str, uint8_t length)
{
	while (length--)
	{
		UART2_send_byte(*str++);
	}
}

void UART2_send_DMA (unsigned char* data, unsigned char length)
{
	while(uart2_transmitter_state != 0);	// wait uart to be free
			// DMA initialization..........
	DMA_InitTypeDef DMA_InitStructure;

	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &USART2->TDR;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) data;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
	DMA_InitStructure.DMA_BufferSize = length;
	
	DMA_Init(DMA1_Channel4,&DMA_InitStructure);
	
	uart2_transmitter_state = 1;	
	DMA_Cmd(DMA1_Channel4,ENABLE);
}

void DMA1_Channel4_5_IRQHandler (void)
{
	if (DMA1->ISR & DMA_ISR_TCIF4)
	{
		DMA1->IFCR |= DMA_IFCR_CTCIF4;	// clear flag
		uart2_transmitter_state = 0;
		DMA_Cmd(DMA1_Channel4,DISABLE);
	}
}