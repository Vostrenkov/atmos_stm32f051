
#include "SIM800_UART.h"
#include "string.h"

struct SIM800_flags
{
	uint8_t OK;
	uint8_t incoming_SMS;
	uint8_t reading_SMS;
	uint8_t need_send;
	uint8_t bar1;
	uint8_t bar2;
	uint8_t balance;
}SIM800;



uint8_t SIM800_send_AT (void)
{
	UART1_send_str((uint8_t*)"AT\r\n");
	Delay_ms(1000);
	if(SIM800.OK)
	{
		SIM800.OK = 0;
		return 1;
	}
	else 
	{
		UART1_send_byte(27);
	}return 0;
}
uint8_t SIM800_send_command (char* command)
{
	UART1_send_str((uint8_t*)command);
	Delay_ms(2000);
	if (SIM800.OK)
	{
		SIM800.OK = 0;
		return 1;		
	}
	else return 0;
}


