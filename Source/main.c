
#include "peripheral.h"
#include "RS485_UART.h"
#include "SIM800_UART.h"
#include "string.h"

#define TWO_BARRELS
#define WARNING_PRESSURE	2.0
#define WARNING_PRESSURE_CLEAR	1.5

uint8_t stat;

char number[][12] = {"+79686279096","+79250066828","+79266752671"};
char tmp_number[12] = "+79686279096";
char message[255];
uint32_t timer_flags;

extern struct SIM800_flags
{
	uint8_t OK;
	uint8_t incoming_SMS;
	uint8_t reading_SMS;
	uint8_t need_send;
	uint8_t bar1;
	uint8_t bar2;
	uint8_t incoming_balance;
}SIM800;

struct UART_struct
{
	uint8_t rx_buf[255];
	uint8_t rx_cnt;
}uart1,uart2;

extern struct RS485_struct
{
	uint8_t waiting_for_answer;
	uint8_t unread_message;
	uint8_t warning;
	float bar1;
	float bar2;
}RS485;

typedef enum 
{
	AT = 1,
	balance = 2,
	modbus_timeout = 3,
	bar1_check = 4,
	bar2_check = 5,
	
}timer_flag_type;

void USART1_IRQHandler (void)		// SIM800 uart interrupt
{
	if (USART1->ISR & USART_FLAG_ORE)
	{
		USART1->ICR |= USART_ICR_ORECF;	
	}
	if (USART1->ISR & USART_FLAG_FE)
	{
		USART1->ICR |= USART_ICR_FECF;	
	}
	if (USART1->ISR & USART_ISR_RXNE)
	{
		uart1.rx_buf[uart1.rx_cnt]=USART1->RDR;
		if (uart1.rx_cnt < 255) uart1.rx_cnt++;
		else uart1.rx_cnt = 0;
		// OK received
		if (uart1.rx_buf[uart1.rx_cnt-4] == 'O' &&
			uart1.rx_buf[uart1.rx_cnt-3] == 'K' &&
			uart1.rx_buf[uart1.rx_cnt-2] == 0x0D &&
			uart1.rx_buf[uart1.rx_cnt-1] == 0x0A)
		{
			if (SIM800.reading_SMS)
			{
				for (int i=0;i<uart1.rx_cnt;i++)
				{
					if (uart1.rx_buf[i] == '+' &&
						uart1.rx_buf[i+1] == '7' &&
						uart1.rx_buf[i+2] == '9'  )
					{
						memcpy(&tmp_number,&uart1.rx_buf[i],12); 
						SIM800.need_send = 1;
					}
				}
				for (int i=0;i<uart1.rx_cnt;i++)	// need pressure
				{
					if ((uart1.rx_buf[i] == 'b' &&
						uart1.rx_buf[i+1] == 'a' &&
						uart1.rx_buf[i+2] == 'r' )||
						(uart1.rx_buf[i] == 'B' &&
						uart1.rx_buf[i+1] == 'a' &&
						uart1.rx_buf[i+2] == 'r' ))
					{
						#ifdef TWO_BARRELS
						SIM800.bar2 = 1;
						#endif
						SIM800.bar1 = 1;
					}
				}
				for (int i=0;i<uart1.rx_cnt;i++)	// need pressure
				{
					if ((uart1.rx_buf[i] == 'b' &&
						uart1.rx_buf[i+1] == 'a' &&
						uart1.rx_buf[i+2] == 'l' &&
						uart1.rx_buf[i+3] == 'a' &&
						uart1.rx_buf[i+4] == 'n' &&
						uart1.rx_buf[i+5] == 'c' &&
						uart1.rx_buf[i+6] == 'e')||
						(uart1.rx_buf[i] == 'B' &&
						uart1.rx_buf[i+1] == 'a' &&
						uart1.rx_buf[i+2] == 'l' &&
						uart1.rx_buf[i+3] == 'a' &&
						uart1.rx_buf[i+4] == 'n' &&
						uart1.rx_buf[i+5] == 'c' &&
						uart1.rx_buf[i+6] == 'e'))
					{
						SetTimer(balance,100);
					}
				}
				SIM800.reading_SMS = 0;
			}
			SIM800.OK = 1;
			uart1.rx_cnt = 0;
		}
		//	balance check
		if (uart1.rx_buf[uart1.rx_cnt-6] == '+' &&
			uart1.rx_buf[uart1.rx_cnt-5] == 'C' &&
			uart1.rx_buf[uart1.rx_cnt-4] == 'U' &&
			uart1.rx_buf[uart1.rx_cnt-3] == 'S' &&
			uart1.rx_buf[uart1.rx_cnt-2] == 'D' &&
			uart1.rx_buf[uart1.rx_cnt-1] == ':' )
		{
			uart1.rx_cnt = 0;
			SIM800.incoming_balance = 1;
		}
		// incoming SMS
		if (uart1.rx_buf[uart1.rx_cnt-6] == '+' &&
			uart1.rx_buf[uart1.rx_cnt-5] == 'C' &&
			uart1.rx_buf[uart1.rx_cnt-4] == 'M' &&
			uart1.rx_buf[uart1.rx_cnt-3] == 'T' &&
			uart1.rx_buf[uart1.rx_cnt-2] == 'I' &&
			uart1.rx_buf[uart1.rx_cnt-1] == ':' )
		{
			uart1.rx_cnt = 0;
			SIM800.incoming_SMS = 1;
		}
		// reading SMS
		if (uart1.rx_buf[uart1.rx_cnt-6] == '+' &&
			uart1.rx_buf[uart1.rx_cnt-5] == 'C' &&
			uart1.rx_buf[uart1.rx_cnt-4] == 'M' &&
			uart1.rx_buf[uart1.rx_cnt-3] == 'G' &&
			uart1.rx_buf[uart1.rx_cnt-2] == 'R' &&
			uart1.rx_buf[uart1.rx_cnt-1] == ':' )
		{
			uart1.rx_cnt = 0;
			SIM800.reading_SMS = 1;
		}
		// incoming RING
		if (uart1.rx_buf[uart1.rx_cnt-22] == '+' &&
			uart1.rx_buf[uart1.rx_cnt-21] == 'C' &&
			uart1.rx_buf[uart1.rx_cnt-20] == 'L' &&
			uart1.rx_buf[uart1.rx_cnt-19] == 'I' &&
			uart1.rx_buf[uart1.rx_cnt-18] == 'P' &&
			uart1.rx_buf[uart1.rx_cnt-17] == ':' )
		{
			for (int n=0;n<10;n++)				// if phone number in database
			{
				for (int i=0;i<uart1.rx_cnt;i++)
				{
					if (uart1.rx_buf[i] == number[n][8] &&
						uart1.rx_buf[i+1] == number[n][9] &&
						uart1.rx_buf[i+2] == number[n][10] &&
						uart1.rx_buf[i+3] == number[n][11] )
					{
						memcpy(&tmp_number,&number[n][0],12); 
						SIM800.need_send = 1;
						#ifdef TWO_BARRELS
						SIM800.bar2 = 1;
						#endif
						SIM800.bar1 = 1;
					}
				}
			}
			uart1.rx_cnt=0;
		}		
	}
}


void USART2_IRQHandler (void)		// RS485 uart interrupt
{
	if (USART2->ISR & USART_FLAG_ORE)
	{
		USART2->ICR |= USART_ICR_ORECF;	
	}
	if (USART2->ISR & USART_FLAG_FE)
	{
		USART2->ICR |= USART_ICR_FECF;	
	}
	if (USART2->ISR & USART_FLAG_IDLE)
	{
		DMA_Cmd(DMA1_Channel5,DISABLE);
		USART2->ICR |= USART_ICR_IDLECF;	// clear flag
		int ticks = 10000;
		while ((DMA1_Channel5->CCR & (1<<0))&&ticks)
		{
			ticks--;
		}
		if (ticks == 0) return; 
		uart2.rx_cnt = (255 - DMA1_Channel5->CNDTR);	// length
		DMA1_Channel5->CNDTR = 255;
		unsigned short crc_in = (uart2.rx_buf[uart2.rx_cnt-2]<<8)|uart2.rx_buf[uart2.rx_cnt-1];	// crc check
		unsigned short crc = 0;
		if (uart2.rx_cnt > 2)	crc	= crc16(uart2.rx_buf,uart2.rx_cnt-2);
		if ((crc_in != crc) || (crc == crc_in == 0)) 
		{
			DMA_Cmd(DMA1_Channel5,ENABLE);
			return;
		}
		RS485.waiting_for_answer = 0;
		RS485.unread_message = 1;
		DMA_Cmd(DMA1_Channel5,ENABLE);
	}
}

void parse_RS485 (void)
{
	uint8_t bar_high;
	uint8_t bar_low;
	uint16_t bar;
	
	if (!RS485.unread_message) return;	
	RS485.unread_message = 0;
	
	switch (uart2.rx_buf[0])
	{
		case 0x02:
			bar_high = uart2.rx_buf[3];
			bar_low = uart2.rx_buf[4];
			bar = bar_high<<8|bar_low;
			RS485.bar1 = (float)bar/100;
		break;
		case 0x10:
			bar_high = uart2.rx_buf[3];
			bar_low = uart2.rx_buf[4];
			bar = bar_high<<8|bar_low;
			RS485.bar2 = (float)bar/100;
		break;
	}
	if (!RS485.warning && (RS485.bar1 >= WARNING_PRESSURE || RS485.bar2 >= WARNING_PRESSURE) )
	{
		RS485.warning = 1;
		SIM800.bar1 = 1;
		SIM800.bar2 = 1;
		SIM800.need_send = 1;	
	}
	if (RS485.warning && (RS485.bar1 <= WARNING_PRESSURE_CLEAR || RS485.bar2 <= WARNING_PRESSURE_CLEAR) )
	{
		RS485.warning = 0;
	}
}

int main (void)
{
	
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq(&RCC_Clocks);
	SysTick_Config(RCC_Clocks.SYSCLK_Frequency/1000);  // setting system 1ms timer
	
	// Init peripherals:
	UART1_config();
	UART2_config();
	GPIO_config();
	
	while (SIM800_send_AT() == 0);		// connect check
	
	// SIM800 initialization:
	while (!SIM800_send_command("AT+COPS?\r\n")); 			// operator 
	while (!SIM800_send_command("AT+CSQ\r\n")); 			// signal
	while (!SIM800_send_command("AT+CMGF=1\r\n")); 			// text mode	
	while (!SIM800_send_command("AT+CSCS=\"GSM\"\r\n")); 	// GSM encoding
	while (!SIM800_send_command("AT+CLIP=1\r\n"));			// AON
	while (!SIM800_send_command("AT+CMGDA=\"DEL ALL\"\r\n")); 			// delete all messages
	LED1_ON;

	SetTimer(bar1_check,2000);
	#ifdef TWO_BARRELS
	SetTimer(bar2_check,6000);
	#endif
	while(1)
	{
		parse_RS485 ();
		if (SIM800.incoming_SMS)
		{
			SIM800.incoming_SMS = 0;
			Delay_ms(100);
			if(SIM800_send_command("AT+CMGR=1\r\n"))
			{
				Delay_ms(1000);
				SIM800_send_command("AT+CMGDA=\"DEL ALL\"\r\n");
			}
		}
		if (SIM800.incoming_balance)
		{
			SIM800.incoming_balance = 0;
			Delay_ms(100);	
			memcpy(message,&uart1.rx_buf[3],uart1.rx_cnt);
			LED2_ON;
			UART1_send_str((uint8_t*)"AT+CMGS=\"");
			UART1_send_str((uint8_t*)tmp_number);
			UART1_send_str((uint8_t*)"\"\r");
			Delay_ms(1000);
			UART1_send_str((uint8_t*)message);
			UART1_send_byte(26);
			SetTimer(AT,10000);
			for (int i=0;i<255;i++) {message[i]=0;}		// clear message buffer
			LED2_OFF;
		}
		if (SIM800.need_send)
		{
			LED2_ON;
			SIM800_send_command("ATH0\r\n");
			Delay_ms(50);
			UART1_send_str((uint8_t*)"AT+CMGS=\"");
			UART1_send_str((uint8_t*)tmp_number);
			UART1_send_str((uint8_t*)"\"\r");
			Delay_ms(1000);
			if (SIM800.bar1)
			{
				SIM800.bar1 = 0;
				char tmp[8];
				sprintf(tmp,"%08f",RS485.bar1);
				UART1_send_str((uint8_t*)"bar1 = ");
				UART1_send_str((uint8_t*)tmp);
				UART1_send_str((uint8_t*)" kg/cm^2\r\n");
			}
			if (SIM800.bar2)
			{
				SIM800.bar2 = 0;
				char tmp[8];
				sprintf(tmp,"%08f",RS485.bar2);
				UART1_send_str((uint8_t*)"bar2 = ");
				UART1_send_str((uint8_t*)tmp);
				UART1_send_str((uint8_t*)" kg/cm^2\r\n");
			}
			UART1_send_byte(26);
			SIM800.need_send = 0;
			SetTimer(AT,10000);
			LED2_OFF;
		}
		if (timer_flags & (1<<AT) && 
				!SIM800.incoming_balance && 
				!SIM800.need_send &&
				!SIM800.incoming_SMS)
		{
			timer_flags &= (1<<AT);
			SIM800_send_AT();
			SetTimer(AT,60000);
		}
		if (timer_flags & (1<<balance) && 
				!SIM800.incoming_balance && 
				!SIM800.need_send &&
				!SIM800.incoming_SMS)
		{
			timer_flags &= (1<<balance);
			SIM800_send_command("AT+CUSD=1,\"#100#\"\r\n");
		}
		if ((timer_flags & (1<<bar1_check)))// && !(timer_flags & (1<<bar2_check)))
		{
			timer_flags &= ~(1<<bar1_check);
			SetTimer(bar1_check,5000);
			RS485_read_reg(0x02,0x00);
		}
		if (timer_flags & (1<<bar2_check))
		{
			timer_flags &= ~(1<<bar2_check);
			Delay_ms(300);
			SetTimer(bar2_check,5000);
			RS485_read_reg(0x10,0x00);
		}		
	}
	return 1;
}
