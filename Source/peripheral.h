
#include "stm32f0xx.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_usart.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_dma.h"

#define UART1_DMA
#define UART2_DMA

#define LED1_ON				GPIOC->ODR |= GPIO_Pin_8
#define LED1_OFF			GPIOC->ODR &= ~GPIO_Pin_8

#define LED2_ON				GPIOC->ODR |= GPIO_Pin_9
#define LED2_OFF			GPIOC->ODR &= ~GPIO_Pin_9

#define Max_Num_Of_Timers		15

static __IO uint32_t TimingDelay;

void Delay_ms(__IO uint32_t nTime);
void SetTimer (unsigned char NewNumber, uint32_t NewTime);
void KillTimer (unsigned char Flag);

void UART1_config (void);
void UART1_send_byte (uint8_t data);
void UART1_send_str (uint8_t* str);

void UART2_config (void);
void UART2_send_byte (uint8_t data);
void UART2_send_str (uint8_t* str, uint8_t length);
void UART2_send_DMA (unsigned char* data, unsigned char length);

void GPIO_config (void);
